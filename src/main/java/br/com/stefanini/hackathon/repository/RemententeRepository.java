package br.com.stefanini.hackathon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.stefanini.hackathon.model.Carta;
import br.com.stefanini.hackathon.model.Remetente;

@Repository

public interface RemententeRepository extends JpaRepository<Remetente, Integer> {

}
