package br.com.stefanini.hackathon.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.stefanini.hackathon.model.Carta;

@Repository

public interface CartaRepository extends JpaRepository<Carta, Integer> {
	@Query("select c from Carta c WHERE c.remnome = :nome")
	public List<Carta> findByNome(@Param("nome") String nome);

}