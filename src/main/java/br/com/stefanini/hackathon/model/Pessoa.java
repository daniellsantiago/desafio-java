package br.com.stefanini.hackathon.model;

import javax.validation.constraints.NotNull;

public abstract class Pessoa {
	
	@NotNull
	private String end;
	private int numContato;
	@NotNull
	private String nome;
	
	
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public int getNumContato() {
		return numContato;
	}
	public void setNumContato(int numContato) {
		this.numContato = numContato;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	
	
	
	
}
