package br.com.stefanini.hackathon.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull; 

@Entity
public class Carta{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String mensagem;
	
	
	@Column
	@NotNull
	private String desnome;
	@NotNull
	private String desEnd;
	
	private int desContato;
	
	@Column
	@NotNull
	private String remnome;
	@NotNull
	private String remEnd;
	
	private int remContato;
	
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}	

	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getDesnome() {
		return desnome;
	}

	public void setDesnome(String desnome) {
		this.desnome = desnome;
	}

	public String getDesEnd() {
		return desEnd;
	}

	public void setDesEnd(String desEnd) {
		this.desEnd = desEnd;
	}

	public int getDesContato() {
		return desContato;
	}

	public void setDesContato(int desContato) {
		this.desContato = desContato;
	}

	public String getRemnome() {
		return remnome;
	}

	public void setRemnome(String remnome) {
		this.remnome = remnome;
	}

	public String getRemEnd() {
		return remEnd;
	}

	public void setRemEnd(String remEnd) {
		this.remEnd = remEnd;
	}

	public int getRemContato() {
		return remContato;
	}

	public void setRemContato(int remContato) {
		this.remContato = remContato;
	}


	
	
}
