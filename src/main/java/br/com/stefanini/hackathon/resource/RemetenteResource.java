package br.com.stefanini.hackathon.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.stefanini.hackathon.model.Remetente;
import br.com.stefanini.hackathon.repository.CartaRepository;
import br.com.stefanini.hackathon.repository.RemententeRepository;

@RestController
@RequestMapping("remetente")
public class RemetenteResource {	
	
	@Autowired
	private RemententeRepository repository;
	
	@PostMapping(value = "/enviar")
	public ResponseEntity<?> enviar(@RequestBody Remetente rem){
		return ResponseEntity.ok(repository.save(rem));
	}
	
	@GetMapping
	public ResponseEntity<List<Remetente>> consultar(){
		return ResponseEntity.ok(repository.findAll());
	}

}
