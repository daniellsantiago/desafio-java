package br.com.stefanini.hackathon.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.stefanini.hackathon.dto.CartaDestDTO;
import br.com.stefanini.hackathon.model.Carta;
import br.com.stefanini.hackathon.repository.CartaRepository;

@RestController
@RequestMapping("cartas")
public class CartaResource {	
	
	@Autowired
	private CartaRepository repository;
	
	@PostMapping(value = "/enviar")
	public ResponseEntity<?> enviar(@RequestBody Carta c){
		return ResponseEntity.ok(repository.save(c));
	}
	
	@GetMapping
	public ResponseEntity<List<Carta>> consultar(){
		return ResponseEntity.ok(repository.findAll());
	}

	@GetMapping(value = "/histDestinatario")
	public ResponseEntity<List<CartaDestDTO>> histDestinario(){
		List<CartaDestDTO> cartas = new ArrayList<CartaDestDTO>();
		List<Carta> cartasAux = repository.findAll();
		
		for (Carta carta : cartasAux) {
			CartaDestDTO c = new CartaDestDTO();
			c.setId(carta.getId());
			c.setMensagem(carta.getMensagem());
			c.setDesnome(carta.getDesnome());
			c.setDesEnd(carta.getDesEnd());
			c.setDesContato(carta.getDesContato());
			cartas.add(c);
		}
		
		
		return ResponseEntity.ok(cartas);
	}
	
	@GetMapping(path = "cartaByNome/{nome}")
	public List<Carta> cartaByNome(@PathVariable("nome") String nome) {
		return repository.findByNome(nome);
	}
	
	
	
}
