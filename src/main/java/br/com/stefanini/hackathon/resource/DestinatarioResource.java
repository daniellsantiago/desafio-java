package br.com.stefanini.hackathon.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.stefanini.hackathon.model.Carta;
import br.com.stefanini.hackathon.model.Destinatario;
import br.com.stefanini.hackathon.repository.CartaRepository;
import br.com.stefanini.hackathon.repository.DestinatarioRepository;

@RestController
@RequestMapping("destinatario")
public class DestinatarioResource {	
	
	@Autowired
	private DestinatarioRepository repository;
	
	@PostMapping(value = "/enviar")
	public ResponseEntity<?> enviar(@RequestBody Destinatario d){
		return ResponseEntity.ok(repository.save(d));
	}
	
	@GetMapping
	public ResponseEntity<List<Destinatario>> consultar(){
		return ResponseEntity.ok(repository.findAll());
	}
	
	
	

}
