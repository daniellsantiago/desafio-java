package br.com.stefanini.hackathon.dto;

public class CartaDestDTO {
	
	private Integer id;
	private String desnome;
	private String mensagem;
	private int desContato;
	private String desEnd;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDesnome() {
		return desnome;
	}
	public void setDesnome(String desnome) {
		this.desnome = desnome;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public int getDesContato() {
		return desContato;
	}
	public void setDesContato(int desContato) {
		this.desContato = desContato;
	}
	public String getDesEnd() {
		return desEnd;
	}
	public void setDesEnd(String desEnd) {
		this.desEnd = desEnd;
	}
	
	
	
}
